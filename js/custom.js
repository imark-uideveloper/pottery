jQuery(window).on("load", function () {
		$(".content").mCustomScrollbar();
	});

jQuery(document).ready(function ($) {

	$('[data-toggle="tooltip"]').tooltip();
	$(".side-nav .collapse").on("hide.bs.collapse", function () {
		$(this).prev().find(".fa").eq(1).removeClass("fa-angle-right").addClass("fa-angle-down");
	});
	$('.side-nav .collapse').on("show.bs.collapse", function () {
		$(this).prev().find(".fa").eq(1).removeClass("fa-angle-down").addClass("fa-angle-right");
	});


	$('ul.nav li a').on('click', function () {

		$(this).siblings().removeClass('active');
		$(this).addClass('active');
	});
	$('ul.nav li').on('click', function () {

		$(this).siblings().removeClass('curunt');
		$(this).addClass('curunt');
	});
	$('ul.nav li ul li a').on('click', function () {

		$(this).removeClass('active');
	});

	if ($('#datepicker').length) {

		$('#datepicker').datepicker({
			uiLibrary: 'bootstrap4'
		});
	}
		if ($('#startdate').length) {

		$('#startdate').datepicker({
			uiLibrary: 'bootstrap4'
		});
	}
		if ($('#enddate').length) {

		$('#enddate').datepicker({
			uiLibrary: 'bootstrap4'
		});
	}
			if ($('#export_details-date_start').length) {

		$('#export_details-date_start').datepicker({
			uiLibrary: 'bootstrap4'
		});
	}
			if ($('#export_details-date_end').length) {

		$('#export_details-date_end').datepicker({
			uiLibrary: 'bootstrap4'
		});
	}
			if ($('#payment_according_start-date').length) {

		$('#payment_according_start-date').datepicker({
			uiLibrary: 'bootstrap4'
		});
	}
			if ($('#payment_according_end-date').length) {

		$('#payment_according_end-date').datepicker({
			uiLibrary: 'bootstrap4'
		});
	}
			if ($('#all_student_start_date').length) {

		$('#all_student_start_date').datepicker({
			uiLibrary: 'bootstrap4'
		});
	}
			if ($('#all_student_end_date').length) {

		$('#all_student_end_date').datepicker({
			uiLibrary: 'bootstrap4'
		});
	}
});